<?php

return [

	/*
	|--------------------------------------------------------------------------
	| oAuth Config
	|--------------------------------------------------------------------------
	*/

	/**
	 * Storage
	 */
	'storage' => 'Session',

	/**
	 * Consumers
	 */
	'consumers' => [

        'Google' => [
            'client_id'     => env('OAUTH_CLIENTID', ''),
            'client_secret' => env('OAUTH_CLIENTSECRET', ''),
            'scope'         => ['userinfo_email', 'userinfo_profile'],
        ],

	]

];