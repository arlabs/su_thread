$(function(){
    var modal_default = $('.modal-default');
    var modal_small = $('.modal-small');
    var modal_body = $('.modal-body-content');
    var edit_thread = $('.edit-thread');
    var delete_message = $('.delete-message');
    var sidebar = $('.sidebar');
    console.log('dd');

    // If modal is closed, do something...
    modal_default.on('hidden.bs.modal', function (e) {
        edit_thread.removeClass('disabled');
        edit_thread.removeAttr('disabled');
    });

    //Edit thread
    edit_thread.on('click', function(e){
        e.preventDefault();

        $(this).addClass('disabled');
        $(this).attr('disabled','disabled');

        var id = $(this).data('id');
        $.ajax({
            method: "GET",
            url: "/threads/"+id+"/get_thread",
            data: {id: id}
        })
            .always(function(result){
                modal_body.html(result);
                modal_default.modal('show');
            });
    });

    //Delete message
    delete_message.on('click',function(e){
        e.preventDefault();

        var id = $(this).data('id');
        $.ajax({
            method: "GET",
            url: "/threads/"+id+"/get_message",
            data: {id: id}
        })
            .always(function(result){
                modal_body.html(result);
                modal_small.modal('show');
            });
    });

    //Tooltip initialization
    $(function () {
        $('[data-toggle="tooltip"]').tooltip({container: 'body'});
    });

    //Disable comment button
    var disable = $('.disable-me');
    disable.on('click',function(){
        $(this).addClass('disabled');
    });
});
