$(document).ready(function(){
    var btn_save = $('.btn_topic');
    var btn_topic = $('.edit_topic');
    var create_topic = $('.createTopic');
    var method = $('#topic_modal form').find('[name=_method]');
    var form = $('#topic_modal form');
    create_topic.on('click',function(){
        $('#modalTopic').modal('show');
        $('#topic_modal form').attr('action', $(this).attr('href'));
        form.find('[name=name]').val('');
        form.find('[name=description]').val('');
        form.find('[name=access]').val('all');
        method.val('POST');
        btn_save.css('back-color','#357ebd');
        $('#modalHeading').html('Create Topic');

    });

    btn_topic.on('click',function(e){
        e.preventDefault();
        var topciName = $(this).data('topic');
        $('#topic_modal form').attr('action', $(this).attr('href'));
        $('#modalHeading').html($(this).data('topic'));
        btn_save.css('border-color','#357ebd');
        var topic = $(this).parents('tr').find('.name').html();
        var description = $(this).parents('tr').find('.description').html();
        var access = $(this).parents('tr').find('.access').html();
        form.find('[name=name]').val(topic);
        form.find('[name=description]').val(description);
        form.find('[name=access]').val(access);
        method.val('PUT');
        $('#modalTopic').modal('show');
    });

    $('#topic_modal form').submit(function (ev) {
        ev.preventDefault();
        btn_save.addClass('disabled');
        btn_save.attr('disabled','disabled');
        var topicName = $(this).find('[name=name]');
        var description = $(this).find('[name=description]').val();
        var access = $(this).find('[name=access]').val();
        topicName.css('border-color', '#ccc');
        if(topicName.val()){
            $.ajax(
                {
                    'url': $(this).attr('action'),
                    'data': {'name': topicName.val(),'description': description,'access': access ,'_method': $(this).find('[name=_method]').val(),'_token': $(this).find('[name=_token]').val()},
                    'type': $(this).attr('method')
                }
            ).done(function(result){
                    location.reload();
                    /*$('.table').find('tbody tr td').html('not yet');*/
                });
        }else{
            btn_save.css("border-color",'#FF3D3D');
            ev.preventDefault();
        }

    });
});