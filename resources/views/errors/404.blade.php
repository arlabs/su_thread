@extends('layouts.login')

@section('content')
    <div class="row">
        <div class="col-md-7">
            <h1>Error 404</h1>

            <h4>We're sorry, the page you are looking for could not be found.</h4>
            <a href="{{ url('/') }}" class="btn btn-warning btn-lg">Go back to Home Page <i class="fa fa-chevron-right"></i></a>
        </div>
        <div class="col-md-5">
            <img src="/images/404.png" class="img-responsive">
        </div>
    </div>
    <?php $is404 = true; ?>
@stop

@section('styles')
<style>
body {
    background: #e6eae1;
    background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPHJhZGlhbEdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgY3g9IjUwJSIgY3k9IjUwJSIgcj0iNzUlIj4KICAgIDxzdG9wIG9mZnNldD0iMCUiIHN0b3AtY29sb3I9IiNlNmVhZTEiIHN0b3Atb3BhY2l0eT0iMSIvPgogICAgPHN0b3Agb2Zmc2V0PSI0MCUiIHN0b3AtY29sb3I9IiNkZmU1ZDciIHN0b3Atb3BhY2l0eT0iMSIvPgogICAgPHN0b3Agb2Zmc2V0PSIxMDAlIiBzdG9wLWNvbG9yPSIjYjliZmFmIiBzdG9wLW9wYWNpdHk9IjEiLz4KICA8L3JhZGlhbEdyYWRpZW50PgogIDxyZWN0IHg9Ii01MCIgeT0iLTUwIiB3aWR0aD0iMTAxIiBoZWlnaHQ9IjEwMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
    background: -moz-radial-gradient(center, ellipse cover,  #e6eae1 0%, #dfe5d7 40%, #b9bfaf 100%);
    background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%,#e6eae1), color-stop(40%,#dfe5d7), color-stop(100%,#b9bfaf));
    background: -webkit-radial-gradient(center, ellipse cover,  #e6eae1 0%,#dfe5d7 40%,#b9bfaf 100%);
    background: -o-radial-gradient(center, ellipse cover,  #e6eae1 0%,#dfe5d7 40%,#b9bfaf 100%);
    background: -ms-radial-gradient(center, ellipse cover,  #e6eae1 0%,#dfe5d7 40%,#b9bfaf 100%);
    background: radial-gradient(ellipse at center,  #e6eae1 0%,#dfe5d7 40%,#b9bfaf 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e6eae1', endColorstr='#b9bfaf',GradientType=1 );

}
</style>
@stop