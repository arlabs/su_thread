<!DOCTYPE html>
<html lang="en">
<head>
    @include('includes.head')
    @yield('styles')
</head>
<body>

    <div class="content-wrapper">
        <div class="container">
            @yield('content')
        </div>
    </div>
    @if(!isset($is404))
    @include('includes.footer')
    @endif

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

    @yield('scripts')
</body>
</html>