<!DOCTYPE html>
<html lang="en">
<head>
    @include('includes.head', ["title" => isset($title) ? $title : "Silliman University Forum"])
    @yield('styles')
</head>
<body>
    {{--@include('includes.head-small')--}}
    @include('includes.head-profile')
    {{--@include('includes.nav')--}}

    <div class="content-wrapper">
        <div class="container">
            {{--<section class="alert">--}}
                {{--<div data-alert="" class="alert-box success">--}}
                    {{--Your email was successfully confirmed.--}}
                {{--</div>--}}
            {{--</section>--}}
            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')
                </div>
            </div>

            @yield('content')
        </div>
    </div>

    @include('includes.footer')

    @include('includes.modals')

    @include('includes.scripts')

    @yield('scripts')

</body>
</html>