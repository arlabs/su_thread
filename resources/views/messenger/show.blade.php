@extends('layouts.master')
@section('content')
    <div class="row main-row">
        @include('messenger.sidebar')

        <div class="col-md-9">
            {{-- Admin Actions --}}
            @if(Auth::check())
                @if(in_array(Auth::user()->account_type, ['admin','moderator']))
                    <div class="pull-right">
                        <div class="btn-group btn-group-sm" role="group" aria-label="...">
                            <!-- Admin -->
                            @if(Auth::user()->account_type == 'admin')
                                <div class="btn-group" role="group">
                                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        Move to: <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        @foreach($topics as $topic)
                                            <li><a href="/threads/change_topic/?thread_id={{ $thread->id }}&topic_id={{ $topic->id }}">{{ $topic->name }}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <!-- // Admin -->

                            <!-- Admin/Moderator -->
                            <a href="#" class="btn btn-success edit-thread" data-id="{{ $thread->id }}"><i class="fa fa-edit"></i> Edit</a>

                            @if($thread->closed)
                                <button type="button" class="btn btn-default active disabled">Thread Closed</button>
                            @else
                                <a href="{{ url('/threads/'.$thread->id.'/close') }}" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Close</a>
                            @endif
                            <!-- // Admin/Moderator -->
                        </div>
                    </div>
                @else
                    <div class="btn-group btn-group-sm pull-right" role="group" aria-label="...">
                        @if($thread->owner_id == Auth::user()->id)
                            <a href="#" class="btn btn-success edit-thread" data-id="{{ $thread->id }}"><i class="fa fa-edit"></i> Edit</a>
                        @endif

                        @if($thread->closed)
                            <span class="btn btn-default active disabled">Thread Closed</span>
                        @endif
                    </div>
                @endif
            @else
                @if($thread->closed)
                    {{-- If not logged in --}}
                    <div class="btn-group btn-group-sm pull-right" role="group" aria-label="...">
                        <span class="btn btn-default active">Thread Closed</span>
                    </div>
                @endif
            @endif
            {{-- End Admin Actions --}}

            <div class="header clearfix">
                <h2>{{ $thread->topic->name }}</h2>
            </div>
            {{--Question--}}
            <div class="thread question">
                <h1>
                    {!! $thread->subject !!}
                </h1>
                <div class="markdown">
                    {!! $bbcode->parse($messages[0]->body, true) !!}
                </div>
                <div class="user">
                    <img src="{{ $messages[0]->user->picture }}" alt="" class="avatar">
                    <div class="info">
                        <h6><a href="/profile/{{ $messages[0]->user->id }}">{{ $messages[0]->user->first_name }} {{ $messages[0]->user->last_name }}</a></h6>
                        <ul class="meta clearfix">
                            <li>{{ $messages[0]->created_at->diffForHumans() }}</li>
                        </ul>
                    </div>
                </div>
                @if(Auth::check())
                    <div class="admin-bar">
                        <ul>
                            <li class="space"></li>
                            <li><a href="#" class="quote">Quote</a></li>
                            @if(in_array(Auth::user()->account_type, ['admin','moderator']))
                                &nbsp;|&nbsp;<li><a href="#">Delete</a></li>
                            @endif
                        </ul>
                    </div>
                @endif
            </div>

            {{--Comments--}}
            <div class="comments">
                @foreach($messages as $key => $message)
                    @if($key > 0)
                    <div class="comment">
                        <div class="user">
                            <img src="{{ $message->user->picture }}" alt="" class="avatar">
                            <div class="info">
                                <h6><a href="/profile/{{ $message->user->id }}">{{ $message->user->first_name }} {{ $message->user->last_name }}</a></h6>
                                <ul class="meta clearfix">
                                    <li>{{ $message->created_at->diffForHumans() }}</li>
                                </ul>
                            </div>
                        </div>
                        <div class="markdown">
                            {!! $bbcode->parse($message->body, true) !!}
                        </div>
                        @if(Auth::check())
                            <div class="admin-bar">
                                <ul>
                                    <li class="space"></li>
                                    <li><a href="#" class="quote _quote_forum_post">Quote</a></li>
                                    @if(in_array(Auth::user()->account_type, ['admin','moderator']))
                                        &nbsp;|&nbsp;<li><a href="#" class="delete-message" data-id="{{ $message->id }}">Delete</a></li>
                                    @endif
                                </ul>
                            </div>
                        @endif
                    </div>
                    @endif
                @endforeach
            </div>

            @if($thread->closed == 0)
            <div class="reply-form">
                {!! Form::open(['url' => ['threads/'.$thread->id.'/comment'], 'method' => 'POST']) !!}
                    <div class="form-row">
                        <label class="field-title">Reply</label>
                        {!! Form::textarea('message', null, ['class' => 'form-control wysiwyg-editor', 'rows' => '5']) !!}
                        {!! $errors->first('message', '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="form-row">
                        <button type="submit" class="btn btn-primary">Reply</button>
                    </div>
                {!! Form::close() !!}
            </div>
            @endif

        </div>
    </div>
@stop