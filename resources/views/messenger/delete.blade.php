{!! Form::open(['route' => [$route, $message->id], 'method' => 'DELETE']) !!}
    <div class="modal-body">
        <p>Are you sure you want to delete this?</p>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-danger">Yes</button>
    </div>
{!! Form::close() !!}