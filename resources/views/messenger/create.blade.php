@extends('layouts.master')

@section('content')
    <div class="row">
        @include('messenger.sidebar')

        <div class="col-md-9">
            <h3>New Thread</h3>
            {!! Form::open(['route' => 'threads.store']) !!}
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading"><strong>Topic:</strong> {{ $topic->name }}</div>

                        <div class="panel-body">
                            <!-- Topic Form Input -->
                            <div class="form-group {{ $errors->has('topic') ? 'has-error' : ''  }}">
                                {!! Form::hidden('topic', $topic->id, ['class' => 'form-control']); !!}
                                {!! $errors->first('topic', '<span class="help-block">:message</span>') !!}
                            </div>

                            <!-- Subject Form Input -->
                            <div class="form-group {{ $errors->has('subject') ? 'has-error' : ''  }}">
                                {!! Form::label('subject', 'Subject', ['class' => 'control-label']) !!}
                                {!! Form::text('subject', null, ['class' => 'form-control']) !!}
                                {!! $errors->first('subject', '<span class="help-block">:message</span>') !!}
                            </div>

                            <!-- Description Form Input -->
                            <div class="form-group {{ $errors->has('description') ? 'has-error' : ''  }}">
                                {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
                                {!! Form::textarea('description', null, ['class' => 'form-control wysiwyg-editor-desc', 'rows' => '3']) !!}
                                {!! $errors->first('description', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">Message</div>

                        <div class="panel-body">
                            <div style="height: 10px;"></div>
                            <!-- Message Form Input -->
                            <div class="form-group {{ $errors->has('message') ? 'has-error' : ''  }}">
                                {!! Form::textarea('message', null, ['class' => 'form-control wysiwyg-editor', 'rows' => '6']) !!}
                                {!! $errors->first('message', '<span class="help-block">:message</span>') !!}
                            </div>

                            <!-- Submit Form Input -->
                            <div class="form-group">
                                {!! Form::submit('Submit', ['class' => 'btn btn-danger btn-sm']) !!}
                            </div>
                        </div>
                    </div>

                    {{--@if($users->count() > 0)--}}
                        {{--<div class="alert alert-danger">--}}
                            {{--<p><strong>Invite other users to this thread:</strong></p>--}}
                            {{--<div class="checkbox">--}}
                                {{--@foreach($users as $user)--}}
                                    {{--<label title="{!! $user->first_name !!} {!! $user->last_name !!}">--}}
                                        {{--<input type="checkbox" name="recipients[]" value="{!! $user->id !!}"> {!! $user->first_name !!}--}}
                                    {{--</label>--}}
                                {{--@endforeach--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--@endif--}}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop