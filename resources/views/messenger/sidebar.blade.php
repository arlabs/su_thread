
<div class="col-md-3" id="sidebar">
    <div id="sidebar-search">
        <input type="text" class="form-control input-search" placeholder="Search a topic">
    </div>
    <ul class="nav nav-sidebar" id="sidebar-list">
        @if($topics->count() > 0)
            <li class="{{ Active::pattern('available_topics') }} sidebar-all"><a href="{{ url('available_topics') }}">All</a></li>
            @foreach($topics as $topic)
                <li class="{{ Active::pattern('available_topics/'.$topic->id) }}">
                    <a href="{{ url('available_topics/'.$topic->id) }}" class="topic-name">{{ $topic->name }} {!! $topic->label !!}</a>
                </li>
            @endforeach
        @else
            <li style="text-align: center;">No topics available</li>
        @endif
    </ul>
</div>

@section('scripts')
<script>
    $(function(){
        //Search for topic
        var sidebar = $('#sidebar');
        var input_search = $('.input-search');
        input_search.on('keyup',function(){
            var topics = sidebar.find('.topic-name');
            $.each(topics, function(){
                var str = $.trim($(this).html().toLowerCase());
                var n = str.search($.trim(input_search.val().toLowerCase()));

                if(n < 0){
                    $(this).parents('li').addClass('hidden');
                }else{
                    $(this).parents('li').removeClass('hidden');
                }
            });
        });
    });
</script>
@stop