@extends('layouts.master')
@section('content')
    <div class="row main-row">
        @include('messenger.sidebar')

        <div class="col-md-9">
            <div class="header clearfix">
                <h1>Forum</h1>
            </div>
            <div class="topics-list threads">

                {{--Template--}}
                {{--<div class="thread-summary question clearfix">--}}
                    {{--<img src="https://avatars.githubusercontent.com/u/9272244?v=2&amp;size=50" alt="Alexius-01">--}}
                    {{--<div class="info clearfix">--}}
                        {{--<h3>--}}
                            {{--<a href="http://laravel.io/forum/05-19-2015-print-page-without-sidebar">--}}
                                {{--Print page without sidebar--}}
                            {{--</a>--}}
                        {{--</h3>--}}

                        {{--<ul class="meta">--}}
                            {{--<li>posted by <a href="http://laravel.io/user/Alexius-01">Alexius-01</a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}

                    {{--<div class="post-info">--}}
                        {{--<a href="http://laravel.io/forum/05-19-2015-print-page-without-sidebar" class="comment-count">0</a>--}}
                    {{--</div>--}}
                {{--</div>--}}




                @if($threads->count() > 0)
                    @foreach($threads as $thread)

                        <div class="thread-summary question clearfix">
                            <img src="{{ $thread->user->picture }}" alt="{{ $thread->user->first_name }} {{ $thread->user->last_name }}">
                            <div class="info clearfix">
                                <h3>
                                    {!! link_to('threads/' . $thread->id, $thread->subject) !!}
                                </h3>

                                <ul class="meta">
                                    <li>posted by <a href="/profile/{{ $thread->user->id }}">{{ $thread->user->first_name }}</a></li>
                                    <li> - latest reply {{$thread->latestMessage->created_at->diffForHumans()}} by <a href="/profile/{{ $thread->latestMessage->user->id }}">{{$thread->latestMessage->user->first_name}} {{$thread->latestMessage->user->last_name}}</a></li>
                                </ul>
                            </div>

                            <div class="post-info">
                                {!! link_to('threads/' . $thread->id, $thread->messagesCount->first()->aggregate, ["class" => "comment-count"]) !!}
                                @if($thread->closed)
                                    <a class="solution closed" href="#"><i class="fa fa-close"></i> closed</a>
                                @endif
                            </div>
                        </div>

                        {{--<div class="panel panel-default" style="{{ $style }}">--}}
                            {{--<div class="panel-body">--}}
                                {{--<small class="pull-right text-muted">--}}
                                    {{--<em>--}}
                                        {{--<span>last reply {!! $thread->latestMessage->created_at->diffForHumans() !!}</span>--}}
                                    {{--</em>--}}
                                {{--</small>--}}
                                {{--<h4>--}}
                                    {{--{!! link_to('threads/' . $thread->id, $thread->subject) !!}--}}
                                    {{--<small> - {!! !empty($thread->latestMessage->body) ? $bbcode->parse(str_limit($thread->latestMessage->body, 50), true) : 'No message' !!}</small>--}}
                                {{--</h4>--}}
                                {{--<em>--}}
                                    {{--<small><strong>Posted by: </strong>{{ $thread->user->first_name }}</small>--}}
                                    {{--<small><strong>Created:</strong> {!! $thread->created_at->diffForHumans() !!}</small>--}}
                                {{--</em>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    @endforeach
                @else
                    <p>No threads available</p>
                @endif
            </div>
        </div>
    </div>
@stop