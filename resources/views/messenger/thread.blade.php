@extends('layouts.master')
@section('content')
    <div class="row main-row">
        @include('messenger.sidebar')


        @if (Session::has('error_message'))
            <div class="alert alert-danger" role="alert">
                {!! Session::get('error_message') !!}
            </div>
        @endif


        <div class="col-md-9">
            @if(Auth::check())
                <a class="btn btn-primary pull-right btn-sm" href="/threads/create?t={{ $topic->id }}">Create Thread</a>
            @endif

            <div class="header clearfix">
                <h1>{{ $topic->name }}</h1>
            </div>

            <div class="topics-list threads">

                @if($threads->count() > 0)
                    @foreach($threads as $thread)

                        <div class="thread-summary question clearfix">
                            <img src="{{ $thread->user->picture }}" alt="{{ $thread->user->first_name }} {{ $thread->user->last_name }}">
                            <div class="info clearfix">
                                <h3>
                                    {!! link_to('threads/' . $thread->id, $thread->subject) !!}
                                </h3>

                                <ul class="meta">
                                    <li>posted by <a href="/profile/{{ $thread->user->id }}">{{ $thread->user->first_name }}</a></li>
                                    <li> - latest reply {{$thread->latestMessage->created_at->diffForHumans()}} by <a href="/profile/{{ $thread->latestMessage->user->id }}">{{$thread->latestMessage->user->first_name}} {{$thread->latestMessage->user->last_name}}</a></li>
                                </ul>
                            </div>

                            <div class="post-info">
                                {!! link_to('threads/' . $thread->id, $thread->messagesCount->first()->aggregate, ["class" => "comment-count"]) !!}
                                @if($thread->closed)
                                    <a class="solution closed" href="#"><i class="fa fa-close"></i> closed</a>
                                @endif
                            </div>
                        </div>
                    @endforeach
                @else
                    <p>No threads available</p>
                @endif
            </div>
        </div>
    </div>
@stop