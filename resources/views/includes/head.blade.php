<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>{{$title or ''}}</title>

<!-- Bootstrap -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
{{--<link rel="stylesheet" href="/template/css/bootstrap.css">--}}

<!-- WYSIWYG -->
<link rel="stylesheet" href="/wysibb/theme/default/wbbtheme.css" type="text/css" />

<!-- FONT AWESOME ICONS  -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<!-- CUSTOM STYLE  -->
{{--<link href="/css/sticky-footer.css" rel="stylesheet"/>--}}
{{--<link href="/template/css/style.css" rel="stylesheet"/>--}}
{{--<link href="/css/thread.css" rel="stylesheet">--}}
<link href="/css/main.css" rel="stylesheet"/>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
{{--<script src="/template/js/jquery-1.11.1.js"></script>--}}
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,300italic,400italic,600italic' rel='stylesheet' type='text/css'>