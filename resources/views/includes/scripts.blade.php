<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/template/js/bootstrap.js"></script>
{{--<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>--}}

<!-- WYSIWYG -->
<script src="/wysibb/jquery.wysibb.min.js"></script>
<script>
    $(document).ready(function() {
        var wbbOpt = {
            buttons: "bold,italic,underline,|,img,link,|,code,quote"
        };
        $(".wysiwyg-editor").wysibb(wbbOpt);

        var desc = {
                    buttons: "bold,italic,underline,|,quote"
                };
        $(".wysiwyg-editor-desc").wysibb(desc);
    });
</script>

<script src="/js/message.js"></script>