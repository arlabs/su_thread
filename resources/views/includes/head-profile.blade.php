<div class="navbar navbar-inverse set-radius-zero" id="top-nav">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">SU Forum</a>
        </div>

        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="{{ Active::pattern(['available_topics','available_topics/*','threads/*']) }}">
                    <a href="/available_topics">Home @include('messenger.unread-count')</a>
                    {{--<a href="#">Home <span class="sr-only">(current)</span></a>--}}
                </li>
                {{--<li><a href="#">Archive</a></li>--}}
                @if(Auth::check())
                    @if(in_array(Auth::user()->account_type, ['admin','moderator']))
                        <li class="{{ Active::pattern('topics') }}"><a href="/topics">Topics</a></li>
                        <li class="{{ Active::pattern('user') }}"><a href="/user">Users</a></li>
                    @endif
                @endif
            </ul>
            @if(Auth::check())
                {{--<ul class="nav navbar-nav navbar-right">--}}
                    {{--<li class="dropdown">--}}
                        {{--<a class="dropdown-toggle clearfix" data-toggle="dropdown" href="#" aria-expanded="false">--}}
                            {{--<div class="avatar pull-left">--}}
                                {{--<img src="{{ Auth::user()->picture }}" />--}}
                            {{--</div>--}}
                            {{--<span class="ellipsis username-ellipsis  pull-left">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</a>--}}
                        {{--</a>--}}
                        {{--<ul class="dropdown-menu" role="menu">--}}
                            {{--<li>--}}
                                {{--<a href="{{ url('auth/logout') }}"><i class="fa fa-sign-out"></i> Logout</a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                {{--</ul>--}}

                <form class="navbar-form navbar-right user-navbar">
                    <a href="#" class="ellipsis username-ellipsis">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</a>
                    <a href="{{ url('auth/logout') }}" class="btn btn-primary btn-sm">Logout</a>

                </form>
            @else
                <form class="navbar-form navbar-right  user-navbar">
                    <a href="/auth/login" class="btn btn-primary btn-sm">Login</a>
                    {{--<li class="dropdown">--}}
                    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>--}}
                    {{--<ul class="dropdown-menu" role="menu">--}}
                    {{--<li><a href="#">Action</a></li>--}}
                    {{--<li><a href="#">Another action</a></li>--}}
                    {{--<li><a href="#">Something else here</a></li>--}}
                    {{--<li class="divider"></li>--}}
                    {{--<li><a href="#">Separated link</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}
                </form>
            @endif
        </div>
    </div>
</div>
