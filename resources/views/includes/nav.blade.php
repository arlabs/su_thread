<section class="menu-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="navbar-collapse collapse ">
                    <ul id="menu-top" class="nav navbar-nav navbar-right">
                        <li>
                            <a href="/available_topics" class="{{ Active::pattern('available_topics') }}">Topics @include('messenger.unread-count')</a>
                        </li>

                        @if(Auth::check())
                            @if(in_array(Auth::user()->account_type, ['admin','moderator']))
                                <li><a href="/topics" class="{{ Active::pattern('topics') }}">Topic Management</a></li>
                                <li><a href="/user" class="{{ Active::pattern('user') }}">User Management</a></li>
                            @endif

                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                                    <span class="avatar">
                                        <img src="{{ Auth::user()->picture }}" />
                                    </span>
                                </a>
                                <div class="dropdown-menu dropdown-settings">
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img src="{{ Auth::user()->picture }}" alt="" class="img-circle" />
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</h4>
                                            <h5><em>{{ ucwords(Auth::user()->account_type) }}</em></h5>
                                        </div>
                                    </div>
                                    <hr />
                                    <a href="{{ url('auth/logout') }}" class="btn btn-danger btn-sm"><i class="fa fa-sign-out"></i> Logout</a>
                                </div>
                            </li>
                        @else
                            <li>
                                <a href="{{ url('auth/login') }}">Login</a>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
