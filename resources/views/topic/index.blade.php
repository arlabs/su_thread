@extends('layouts.master')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="media">
            <p><button class="btn btn-success pull-right createTopic" href="/topics">Create</button></p>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Visibility</th>
                        <th>Created At</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($topics as $topic)
                    <tr>
                        <td class="name">{!! $topic->name !!}</td>
                        <td class="description">{!! $topic->description !!}</td>
                        <td class="access">{!! ucwords($topic->access) !!}</td>
                        <td>{!! date('d F Y h:i a', strtotime($topic->created_at)) !!}</td>
                        <td>
                            <a href="/topics/{{ $topic->id }}" class="btn btn-danger btn-sm edit_topic"
                            data-id="{{ $topic->id }}" data-topic="{!! $topic->name !!}" data-toggle="modal"
                            data-target="#editTopic"><i class="fa fa-edit"></i> Edit</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {!! $topics->render() !!}
        </div>

        <!-- Modal -->
        <div class="modal fade" id="modalTopic" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content" id="topic_modal">
                    {!! Form::open(array('url' => '', 'method' => 'put')) !!}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="modalHeading">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            {!! Form::label('name','Topic:') !!}
                            {!! Form::text('name', null, array('class' => 'form-control')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('description','Description:') !!}
                            {!! Form::textarea('description','',array('class'=>'form-control', 'rows'=>'5')) !!}
                        </div>

                        @if(Auth::user()->account_type == 'admin')
                        <div class="form-group">
                            {!! Form::label('access','Visibility:') !!}
                            {!! Form::select('access', array('admin' => 'Admin', 'all' => 'All'),'all',array('class' => 'form-control')) !!}
                        </div>
                        @endif
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success btn_topic">Save</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@stop

@section('scripts')
    <script src="/js/topic.js"></script>
@stop