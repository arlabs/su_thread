@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="media">
                <p><button class="btn btn-success pull-right createUser">Create</button></p>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>User Type</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th>Created At</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($allUser as $user)
                        <tr>
                            <td class="name">{{ $user->first_name.' '.$user->last_name }}</td>
                            <td class="access">{!! ucwords($user->account_type) !!}</td>
                            <td class="access">{!! $user->email !!}</td>
                            <td class="access">{!! ucwords($user->status) !!}</td>
                            <td class="access">{!! date('d F Y h:i a', strtotime($user->created_at)) !!}</td>
                            <td><a href="/user/{{ $user->id }}" class="btn btn-danger btn-sm edit_topic" data-id="{{ $user->id }}" data-topic="{!! $user->name !!}" data-toggle="modal" data-target="#editTopic"><i class="fa fa-edit"></i> Edit</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $allUser->render() !!}
            </div>
        </div>
    </div>

    <script>
        $(function(){
            var modal_default = $('.modal-default');
            var modal_default_body = $('.modal-body-content');
            var modal_title = $('.modal-title');
            var user_edit = $('.edit_topic');
            var user_create = $('.createUser');
            user_create.on('click', function(e){
                e.preventDefault();

                $.ajax({
                    method: "GET",
                    url: "/user/user_modal",
                    data: {'option': 'create'}
                })
                .always(function(result){
                    modal_title.html('Create User');
                    modal_default_body.html(result);
                    modal_default.modal('show');
                });
            });

            user_edit.on('click', function(e){
                e.preventDefault();
                var userId = $(this).data('id');
                $.ajax({
                    method: "GET",
                    url: "/user/user_modal",
                    data: {'option': 'edit','userId': userId}
                })
                .always(function(result){
                    modal_title.html('Edit User');
                    modal_default_body.html(result);
                    modal_default.modal('show');
                });

            });
        });
    </script>
@stop