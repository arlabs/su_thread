@if($option == 'create')
    {!! Form::open(['route' => ['user.store']]) !!}
@elseif($option == 'edit')
    {!! Form::open(['route' => ['user.edit',$user->id]]) !!}
@endif

<div class="modal-body">
    <!-- Subject Form Input -->
    <div class="form-group">
        {!! Form::label('first_name', 'First name', ['class' => 'control-label']) !!}
        {!! Form::text('first_name', isset($user->first_name)? $user->first_name : '', ['class' => 'form-control input','placeholder' => 'First name']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('last_name', 'Last name', ['class' => 'control-label']) !!}
        {!! Form::text('last_name', isset($user->last_name)? $user->last_name : '', ['class' => 'form-control input','placeholder' => 'Last name']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('email', 'Username', ['class' => 'control-label']) !!}
        {!! Form::text('email', isset($user->email)? $user->email : '', ['class' => 'form-control input','placeholder' => 'Username']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('gender', 'Gender', ['class' => 'control-label']) !!}
        {!! Form::select('gender', array('male' => 'Male', 'female' => 'Female'),isset($user->gender)? $user->gender : '' ,array('class' => 'form-control input')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('account_type', 'Account Type', ['class' => 'control-label']) !!}
        {!! Form::select('account_type', array('admin' => 'Admin', 'moderator' => 'Moderator','student' => 'Student'),isset($user->account_type)? $user->account_type : 'student',array('class' => 'form-control input')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('status', 'Status', ['class' => 'control-label']) !!}
        {!! Form::select('status', array('active' => 'Active', 'inactive' => 'Inactive'),isset($user->status)? $user->status : '' ,array('class' => 'form-control input')) !!}
    </div>

</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-primary">Save</button>
</div>
{!! Form::close() !!}

<script>
    $(document).ready(function() {
        $('form').submit(function(e){
            e.preventDefault();
            var userInput = $('.input')
            var userData = {};
            $('.user_errors').remove();
            var token = $(this).find('[name=_token]').val();
            $(userInput).each(function(e,v){
                userData[$(this).attr('name')] = $(this).val();
            });
            $.ajax(
                    {
                'url': $(this).attr('action'),
                'data': {'first_name': userData.first_name,'last_name': userData.last_name,'email': userData.email,'gender':userData.gender,'account_type': userData.account_type,'status': userData.status,'_token': token},
                'type': 'POST'
            }
            ).done(function(result){
                if(result.errors){
                var form = $('form');
                var labels = form.find('input');
                labels.each(function(e, v){
                    var field = $(this).attr('name');
                    if($.inArray(field, result.errors)){
                        if(field == 'first_name'){
                            result.errors.first_name? $(this).closest('.form-group').children('label').append(' <span class="user_errors label label-danger">'+result.errors.first_name+'</span>') : '';
                        }else if(field == 'last_name'){
                            result.errors.last_name? $(this).closest('.form-group').children('label').append(' <span class="user_errors label label-danger">'+result.errors.last_name+'</span>') : '';
                        }else if(field == 'email'){
                            result.errors.email? $(this).closest('.form-group').children('label').append(' <span class="user_errors label label-danger">'+result.errors.email+'</span>') : '';
                        }

                    }
                });
                }else{
                    location.reload();
//                    $('.notify').removeClass('hidden');
//                    $('.notify').show();
//                    $('.notify_msg').html('<strong>Congratulations!</strong> you already added a new user');
//                    $('.notify').delay(1500).fadeOut();
                }
            });
        });
    });
</script>
