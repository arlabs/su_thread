@extends('layouts.login')

@section('styles')
    <link rel="stylesheet" href="/css/login.css">
@stop

@section('content')
    <img src="/images/login-bg.jpg" class="bg"/>

    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-container">
                <h3>Join the forum!</h3>
                <a href="{{ url('google_login') }}" class="btn btn-danger btn-block btn-lg">
                    <i class="fa fa-google-plus"></i> &nbsp; Login with Google
                </a>
            </div>
        </div>
    </div>
@endsection
