<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model {

    protected $fillable = ['name', 'order_by'];

    public static function getAllLatest()
    {
        return self::latest('updated_at');
    }

    public function threads(){
        return $this->hasMany("Cmgmyr\Messenger\Models\Thread");
    }

}
