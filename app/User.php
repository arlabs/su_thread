<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Cmgmyr\Messenger\Traits\Messagable;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

    use Messagable;
	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['first_name', 'last_name', 'email', 'link', 'picture', 'gender', 'account_type', 'status', 'remember_token'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

    public static function registerUser($request){
        $user = new User();

        $user->first_name = $request['given_name'];
        $user->last_name = $request['family_name'];
        $user->email = $request['email'];
        $user->link = isset($request['link']) ? $request['link'] : '';
        $user->picture = $request['picture'];
        $user->gender = $request['gender'];
        $user->save();

        return $user;
    }

}
