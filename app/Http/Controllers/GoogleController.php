<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\Flash;

class GoogleController extends Controller {

    public function loginWithGoogle(Request $request){
        // get data from request
        $code = $request->get('code');

        // get google service
        $googleService = \OAuth::consumer('Google');

        // check if code is valid

        // if code is provided get user data and sign in
        if ( ! is_null($code))
        {
            // This was a callback request from google, get the token
            $token = $googleService->requestAccessToken($code);

            // Send a request with it
            $result = json_decode($googleService->request('https://www.googleapis.com/oauth2/v1/userinfo'), true);

            if(ends_with($result['email'], "gmail.com")){ // change gmail.com to su.edu.ph
                $existing_user = User::where("email", $result['email'])->first();
                if($existing_user == null){
                    $existing_user = User::registerUser($result);
                }
                $user = User::find($existing_user->id);

                Auth::login($user);

                return redirect('available_topics');
            }else{
                Flash::error('Sorry, you are not allowed to access this site.');
                return Redirect::back();
            }
        }
        // if not ask for permission first
        else
        {
            // get googleService authorization
            $url = $googleService->getAuthorizationUri();

            // return to google login url
            return redirect((string)$url);
        }
    }

}
