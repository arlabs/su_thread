<?php namespace App\Http\Controllers;


use App\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class TopicController extends Controller {

    public function __construct(){
        $check = Auth::user()->account_type == 'admin'? true: false;
        if(!$check){
            return Redirect::route('available_topics');
        }
    }

    public function index(){
        $topics = Topic::getAllLatest()->paginate(10);
        return view('topic.index', compact('topics'));
    }

    public function update(Request $request, $topicId){
        $topic = New Topic();

        $topic
            ->where('id', $topicId)
            ->update(['name' => $request->get('name'), 'description' => $request->get('description'), 'access' => $request->get('access')]);

        $topics = Topic::getAllLatest()->paginate(10)->toJson();

        return $topics;
    }

    public function store(Request $request){
        $topic = New Topic();

        $topic->name = $request->get('name');
        $topic->description = $request->get('description');
        $topic->access = $request->get('access');
        $topic->save();

        return $topic;
    }

}