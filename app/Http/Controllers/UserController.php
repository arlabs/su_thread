<?php namespace App\Http\Controllers;



use App\Http\Requests\CreateUserRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class UserController extends Controller{


    public function index(){
        $user = New User();

        $allUser = User::where('account_type', '!=', 'admin')->paginate(10);

        return view('user.index', compact('allUser'));
    }

    public function user_modal(Request $request){
        $option = $request->get('option');

        if($request->get('userId')){
            $user = User::find($request->get('userId'));
            $view = View::make('user._create_user_modal',compact('option','user'));
        }else{
            $view = View::make('user._create_user_modal',compact('option'));
        }

        return $view->render();
    }

    public function store(Request $request){

        if ($request->ajax()){
            $v = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|unique:users,email'
            ]);
            if ($v->fails())
            {
                $error['errors'] = $v->errors();
                return $error;
            }else{
                $user = New User();
                Input::merge(array('email' => str_contains($request->get('email'), '@su.edu.ph')? $request->get('email') : $request->get('email').'@su.edu.ph'));
                $thisUser = $user->create($request->all());
                $allUser = User::where('account_type', '!=', 'admin')->paginate(10);
                return $allUser;
            }
        }
    }

    public function update(Request $request, $userId){
        if ($request->ajax()){
            $v = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required'
            ]);
            if ($v->fails())
            {
                $error['errors'] = $v->errors();
                return $error;
            }else{
                $user = User::find($userId);
                $user->first_name = $request->get('first_name');
                $user->last_name = $request->get('last_name');
                $user->email = str_contains($request->get('email'), '@su.edu.ph')? $request->get('email') : $request->get('email').'@su.edu.ph';
                $user->gender = $request->get('gender');
                $user->account_type = $request->get('account_type');
                $user->status = $request->get('status');
                $user->save();
                $allUser = User::where('account_type', '!=', 'admin')->paginate(10);
                return $allUser;
            }
        }
    }

}