<?php namespace App\Http\Controllers;
use App\Topic;
use App\User;
use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Thread;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Golonka\BBCode\BBCodeParser;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Laracasts\Flash\Flash;

class MessagesController extends Controller
{
    private $topics;
    private $account_type;
    private $bbcode;

    /**
     * Just for testing - the user should be logged in. In a real
     * app, please use standard authentication practices
     */
    public function __construct()
    {
        if(Auth::check() && Auth::user()->account_type == 'admin'){
            $this->topics = Topic::all();
        }else{
            $this->topics = Topic::where('access','all')->get();
        }

//        $currentUserId = Auth::check() ? Auth::user()->id : '';
//
//        foreach($topics as $topic){
//            $topic->thread = Thread::getAllLatest()->where('topic_id',$topic->id)->get();
//            foreach($topic->thread as $thread){
//                if($thread->closed == 1){
//                    if($thread->isUnread($currentUserId)){
//                        $topic->label = '<span class="badge pull-right badge-danger">Closed</span>';
//                        break;
//                    }
//                }else{
//                    if($thread->isUnread($currentUserId)){
//                        $topic->label = '<span class="badge pull-right badge-danger"><i class="fa fa-comments"></i></span>';
//                        break;
//                    }
//                }
//
//                $topic->topic_id = $thread->topic_id;
//            }
//        }
//        $this->topics = $topics;

        $this->bbcode = new BBCodeParser;
    }
    /**
     * Show all of the message threads to the user
     *
     * @return mixed
     */
    public function index()
    {
        $topics = $this->topics;

        //PLEASE READ :
        //pagination : where's the pagination?? the old query fetches all threads from the database. What if there are 1000+ threads?
        //access : don't need foreach loop, just use whereHas
        //sorting : sort by latest and by sticky
        //count message : see Thread::messagesCount
        //$thread = Thread::getAllLatest()->get();

        //get all latest threads  and load topics, user and message count of the thread ahead of time (optimization)
        $threads = Thread::getAllLatest()->with(["topic", "user", "messagesCount", "latestMessage", "latestMessage.user"]);

        //filter threads by its topic's access permission
        if(Auth::check() && Auth::user()->account_type == 'admin'){
            //no filter for admins
        }else{
            $threads = $threads->whereHas("topic", function($i){
                $i->where("access", "all");
            });
        }

        //paginate threads, limit query to 15 per page (might change)
        $threads = $threads->paginate(15);

//        $threads = array();
//        if(Auth::check() && Auth::user()->account_type == 'admin'){
//            $threads = $thread;
//        }else{
//            if($thread->count()){
//                foreach ($thread as $t) {
//                    $topic = Topic::where('id', $t->topic_id)->first();
//                    if($topic->access == 'all'){
//                        $threads[] = $t;
//                    }
//                }
//            }
//        }

        $bbcode = $this->bbcode;

        return view('messenger.index', compact('topics','threads','bbcode'));
    }

    public function showThread($id){
        $topics = $this->topics;
        $currentUserId = Auth::check() ? Auth::user()->id : '';

        try{
            if(Auth::check() && Auth::user()->account_type == 'admin'){
                $topic = Topic::where('id', $id)->firstOrFail();
            }else{
                $topic = Topic::where('id', $id)->where('access', 'all')->firstOrFail();
            }
            if($topic == null){
                throw new ModelNotFoundException();
            }
        }catch (ModelNotFoundException $e){
            return redirect('restricted');
        }

        // All threads, ignore deleted/archived participants
        $threads = Thread::getAllLatest()->with(["topic", "user", "messagesCount", "latestMessage", "latestMessage.user"])->where('topic_id', $id)->paginate(15);

        // All threads that user is participating in
        // $threads = Thread::forUser($currentUserId)->latest('updated_at')->get();

        // All threads that user is participating in, with new messages
        // $threads = Thread::forUserWithNewMessages($currentUserId)->latest('updated_at')->get();

        $bbcode = $this->bbcode;
        return view('messenger.thread', compact('topic', 'topics', 'threads', 'bbcode'));
    }
    /**
     * Shows a message thread
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        try {
            $thread = Thread::findOrFail($id);
            $messages = $thread->messages()->paginate(10);
        } catch (ModelNotFoundException $e) {
            return redirect('restricted');
        }
        // show current user in list if not a current participant
        // $users = User::whereNotIn('id', $thread->participantsUserIds())->get();
        // don't show the current user in list
        $userId = Auth::check() ? Auth::user()->id : '';
        //$users = User::whereNotIn('id', $thread->participantsUserIds($userId))->get();
        $thread->markAsRead($userId);

        $topics = $this->topics;

        //BBCode Parser
        $bbcode = $this->bbcode;

        return view('messenger.show', compact('thread', 'messages', 'topics', 'string','bbcode'));
    }
    /**
     * Creates a new message thread
     *
     * @return mixed
     */
    public function create(Request $request)
    {
        if(isset($request) && $request->has('t') && $request->get('t') != null) {
            $topic = Topic::find($request->get('t'));
        }else{
            return redirect('available_topics');
        }

        $topics = $this->topics;

        //$users = User::where('id', '!=', Auth::id())->get();
        return view('messenger.create', compact('users','topic','topics'));
    }
    /**
     * Stores a new message thread
     *
     * @return mixed
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'topic' => 'required',
                'subject' => 'required',
                'description' => 'required',
                'message' => 'required'
            ]);
        if($validator->fails()){
            return Redirect::intended('threads/create?t=1')->withErrors($validator)->withInput();
        }else{
            $input = $request->all();
            $thread = Thread::create(
                [
                    'subject' => $input['subject'],
                    'description' => $input['description'],
                    'topic_id' => $input['topic'],
                    'pinned' => 0,
                    'closed' => 0,
                    'owner_id' => Auth::user()->id
                ]
            );
            // Message
            Message::create(
                [
                    'thread_id' => $thread->id,
                    'user_id'   => Auth::user()->id,
                    'body'      => $input['message'],
                ]
            );
            // Sender
            Participant::create(
                [
                    'thread_id' => $thread->id,
                    'user_id'   => Auth::user()->id,
                    'last_read' => new Carbon
                ]
            );
            // Recipients
            if (Input::has('recipients')) {
                $thread->addParticipants($input['recipients']);
            }
        }

        Flash::success('Thread successfully created!');

        return redirect('available_topics/'.$input['topic']);
    }
    /**
     * Adds a new message to a current thread
     *
     * @param $id
     * @return mixed
     */
    public function comment($id, Request $request)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return redirect('restricted');
        }

        //Validate inputs
        $validator = Validator::make($request->all(),
            [
                'message' => 'required'
            ]
        );
        if($validator->fails()){
            return Redirect::intended('threads/'.$id.'#comment-form')->withErrors($validator)->withInput();
        }else{
            $thread->activateAllParticipants();
            // Message
            Message::create(
                [
                    'thread_id' => $thread->id,
                    'user_id'   => Auth::user()->id,
                    'body'      => Input::get('message'),
                ]
            );
            // Add replier as a participant
            $participant = Participant::firstOrCreate(
                [
                    'thread_id' => $thread->id,
                    'user_id'   => Auth::user()->id
                ]
            );
            $participant->last_read = new Carbon;
            $participant->save();

            // Recipients
            if (Input::has('recipients')) {
                $thread->addParticipants(Input::get('recipients'));
            }
        }

        Flash::success('Comment successfully posted!');

        return Redirect::back();
    }

    public function change_topic(Request $request){
        try{
            $thread = Thread::findOrFail($request->get('thread_id'));
        }catch (ModelNotFoundException $e){
            return redirect('restricted');
        }

        $thread->topic_id = $request->get('topic_id');
        $thread->save();

        Flash::success('Thread successfully moved!');

        return redirect('available_topics/' . $request->get('topic_id'));
    }

    public function close($id){
        try{
            $thread = Thread::findOrFail($id);
        }catch (ModelNotFoundException $e){
            return redirect('restricted');
        }

        $thread->closed = 1;
        $thread->save();

        Flash::success('Thread successfully closed!');

        return Redirect::back();
    }

    public function get_message($id){
        try{
            $message = Message::findOrFail($id);
        }catch (ModelNotFoundException $e){
            return redirect('restricted');
        }

        $view = View::make('messenger.delete',['message' => $message, 'route' => 'threads.destroy_message']);
        $content = $view->render();

        return $content;
    }

    public function destroy_message($id){
        try{
            $message = Message::findOrFail($id);
        }catch (ModelNotFoundException $e){
            return redirect('restricted');
        }

        $message->delete();

        Flash::success('Message successfully deleted!');

        return Redirect::back();
    }

    public function get_thread($id){
        try{
            $thread = Thread::findOrFail($id);
        }catch (ModelNotFoundException $e){
            return redirect('restricted');
        }

        $view = View::make('messenger.edit',['thread' => $thread]);
        $content = $view->render();

        return $content;
    }

    public function update($id, Request $request){
        try{
            $thread = Thread::findOrFail($id);
        }catch (ModelNotFoundException $e){
            return redirect('restricted');
        }

        $thread->subject = $request->get('subject');
        $thread->description = $request->get('description');
        $thread->save();

        Flash::success('Thread successfully updated!');

        return redirect('threads/' . $id);
    }
}