<?php

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::get('google_login', 'GoogleController@loginWithGoogle');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);


//region Messages
Route::group(['prefix' => 'available_topics'], function(){
    Route::get('/', 'MessagesController@index');
    Route::get('{id}', 'MessagesController@showThread');
});
Route::group(['prefix' => 'threads'], function () {
    Route::get('change_topic', 'MessagesController@change_topic');
    Route::get('create', 'MessagesController@create');
    Route::post('/', ['as' => 'threads.store', 'uses' => 'MessagesController@store']);
    Route::get('{id}', ['as' => 'threads.show', 'uses' => 'MessagesController@show']);
    Route::post('{id}/comment', ['as' => 'threads.comment', 'uses' => 'MessagesController@comment']);
    Route::get('{id}/close', ['as' => 'threads.close', 'uses' => 'MessagesController@close']);
    Route::delete('{id}', ['as' => 'threads.destroy_message', 'uses' => 'MessagesController@destroy_message']);
    Route::get('{id}/get_thread', ['as' => 'threads.get_thread', 'uses' => 'MessagesController@get_thread']);
    Route::put('{id}', ['as' => 'threads.update', 'uses' => 'MessagesController@update']);
    Route::get('{id}/get_message', ['as' => 'threads.get_message', 'uses' => 'MessagesController@get_message']);
});
//endregion


Route::group(['middleware' => 'auth'], function(){
    //region Topics
    Route::group(['prefix' => 'topics'],function(){
        Route::get('/', ['as' => 'topics', 'uses' => 'TopicController@index']);
        Route::get('create', ['as' => 'topics.create', 'uses' => 'TopicController@create']);
        Route::post('/', ['as' => 'topics.store', 'uses' => 'TopicController@store']);
        Route::get('{id}', ['as' => 'topics.show', 'uses' => 'TopicController@show']);
        Route::put('{id}', ['as' => 'topics.update', 'uses' => 'TopicController@update']);
    });
    //endregion

    //region UserManagement
    Route::group(['prefix' => 'user'],function(){
        Route::get('/',['as' => 'user', 'uses' => 'UserController@index']);
        Route::post('/',['as' => 'user.store', 'uses' => 'UserController@store']);
        Route::post('/user/{id}',['as' => 'user.edit', 'uses' => 'UserController@update']);
        Route::get('user_modal',['as' => 'user.user_modal', 'uses' => 'UserController@user_modal']);
    });
    //endregion
});